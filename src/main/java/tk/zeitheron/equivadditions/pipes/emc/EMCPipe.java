package tk.zeitheron.equivadditions.pipes.emc;

import tk.zeitheron.equivadditions.EquivalentAdditions;
import tk.zeitheron.equivadditions.pipes.IPipe;
import tk.zeitheron.equivadditions.tiles.TileEMCPipe;

import moze_intel.projecte.api.tile.IEmcAcceptor;
import moze_intel.projecte.api.tile.IEmcProvider;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public abstract class EMCPipe
{
	protected PipeEMCStorage buffer;
	public EMCPipeGrid grid;
	public final TileEMCPipe tile;
	
	public float delta;
	
	public EMCPipe(TileEMCPipe tile)
	{
		this.tile = tile;
	}
	
	public PipeEMCStorage getBuffer()
	{
		if(buffer == null)
			buffer = new PipeEMCStorage(getMaxTransfer());
		return buffer;
	}
	
	public boolean isValid()
	{
		return tile != null && !tile.isInvalid();
	}
	
	public abstract ResourceLocation getPipeTexture();
	
	public abstract int getMaxTransfer();
	
	public void update()
	{
		long emc = getBuffer().energy;
		long start = System.currentTimeMillis();
		linkGrid();
		updateDelta();
		splitEnergy();
		emitEnergy();
		long end = System.currentTimeMillis();
		if(emc != getBuffer().energy)
			tile.setTooltipDirty(true);
		if(end - start >= 10L)
			EquivalentAdditions.LOG.debug("Tick for " + tile.getPos() + " took longer than expected (" + (end - start) + " ms)");
	}
	
	public void linkGrid()
	{
		if(grid == null)
		{
			// Try to connect to nearest pipes, if possible
			for(EnumFacing face : EnumFacing.VALUES)
			{
				EMCPipe p = tile.getRelPipe(face);
				if(p != null && p.grid != null)
				{
					p.grid.addPipe(this);
					grid = p.grid;
					break;
				}
			}
			
			if(grid == null)
				grid = new EMCPipeGrid(tile.getWorld(), this);
		} else
			for(EnumFacing face : EnumFacing.VALUES)
			{
				EMCPipe p = tile.getRelPipe(face);
				if(p != null && p.grid != null && p.grid != grid && p.grid.positions.size() >= grid.positions.size())
				{
					EMCPipeGrid.merge(p.grid, grid);
					break;
				}
			}
	}
	
	protected void emitEnergy()
	{
		for(EnumFacing face : EnumFacing.VALUES)
		{
			TileEntity te = tile.getWorld().getTileEntity(tile.getPos().offset(face));
			if(te != null && !(te instanceof IPipe))
			{
				if(te instanceof IEmcProvider)
				{
					IEmcProvider es = (IEmcProvider) te;
					if(es != null)
					{
						if(te instanceof IEmcAcceptor)
						{
							IEmcAcceptor ea = (IEmcAcceptor) es;
							
							double percent = es.getStoredEmc() / (double) es.getMaximumEmc();
							double ourPercent = getBuffer().getStoredEmc() / (double) getBuffer().getMaximumEmc();
							double avg = (percent + ourPercent) / 2.;
							
							if(percent - ourPercent > 0)
							{
								double in = avg - ourPercent;
								long emc = Math.round(in * getBuffer().getMaximumEmc());
								getBuffer().receiveEnergy(es.provideEMC(face.getOpposite(), getBuffer().receiveEnergy(emc, true)), false);
							} else if(ourPercent - percent > 0)
							{
								double in = avg - percent;
								long emc = Math.round(in * getBuffer().getMaximumEmc());
								getBuffer().extractEnergy(ea.acceptEMC(face.getOpposite(), getBuffer().extractEnergy(emc, true)), false);
							}
						} else
						{
							long maxExtract = getBuffer().getMaxEnergyStored() - getBuffer().getEnergyStored();
							getBuffer().receiveEnergy(es.provideEMC(face.getOpposite(), maxExtract), false);
						}
					}
				} else if(te instanceof IEmcAcceptor)
				{
					IEmcAcceptor es = (IEmcAcceptor) te;
					if(es != null)
					{
						long maxReceive = getBuffer().getEnergyStored();
						getBuffer().extractEnergy(es.acceptEMC(face.getOpposite(), maxReceive), false);
					}
				}
			}
		}
		
		// double energy =
		// tile.acessPoint.emitEnergy(getBuffer().getEnergyStored());
		// delta += energy;
		// getBuffer().extractEnergy(energy, false);
	}
	
	protected void updateDelta()
	{
		delta -= delta / 20F;
		if(delta <= .001F)
			delta = 0;
	}
	
	protected void splitEnergy()
	{
		if(grid != null && grid.isMaster(tile.getPos()))
			grid.balance();
	}
	
	public void setEnergy(long energy)
	{
		long pr = getBuffer().getEnergyStored();
		getBuffer().setEnergyStored(energy);
		delta += Math.abs(pr - getBuffer().getEnergyStored());
	}
	
	public void read(NBTTagCompound nbt)
	{
		getBuffer().readFromNBT(nbt);
	}
	
	public void write(NBTTagCompound nbt)
	{
		getBuffer().writeToNBT(nbt);
	}
	
	public long receiveEnergy(long maxReceive, boolean simulate)
	{
		return canReceive() ? getBuffer().receiveEnergy(maxReceive, simulate) : 0;
	}
	
	public long extractEnergy(long maxExtract, boolean simulate)
	{
		return getBuffer().extractEnergy(maxExtract, simulate);
	}
	
	public long getEnergyStored()
	{
		return getBuffer().getEnergyStored();
	}
	
	public long getMaxEnergyStored()
	{
		return getBuffer().getMaxEnergyStored();
	}
	
	public boolean canExtract()
	{
		return getBuffer().canExtract();
	}
	
	public boolean canReceive()
	{
		return true;
	}
}