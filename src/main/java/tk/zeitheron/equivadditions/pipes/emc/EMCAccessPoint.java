package tk.zeitheron.equivadditions.pipes.emc;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.FluidHelper;

import moze_intel.projecte.api.tile.IEmcAcceptor;
import moze_intel.projecte.api.tile.IEmcProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.wrappers.BlockLiquidWrapper;
import net.minecraftforge.fluids.capability.wrappers.FluidBlockWrapper;

public class EMCAccessPoint
{
	World world;
	BlockPos pos;
	
	public EMCAccessPoint(World world, BlockPos pos)
	{
		this.world = world;
		this.pos = pos;
	}
	
	public static FluidEnergyAccessPoint create(World world, BlockPos pos)
	{
		return new FluidEnergyAccessPoint(world, pos);
	}
	
	public IndexedMap<EnumFacing, IEmcAcceptor> findEMCAcceptors()
	{
		IndexedMap<EnumFacing, IEmcAcceptor> list = new IndexedMap<>();
		for(EnumFacing face : EnumFacing.VALUES)
		{
			BlockPos apos = pos.offset(face);
			TileEntity tile = world.getTileEntity(apos);
			if(tile instanceof IEmcAcceptor && tile instanceof IEmcProvider)
				continue;
			if(tile instanceof IEmcAcceptor && (((IEmcAcceptor) tile).getMaximumEmc() - ((IEmcAcceptor) tile).getStoredEmc() > 1.0E-4))
				list.put(face.getOpposite(), (IEmcAcceptor) tile);
		}
		return list;
	}
	
	public List<IFluidHandler> findFLAcceptors(FluidStack fluid)
	{
		List<IFluidHandler> list = new ArrayList<>();
		for(EnumFacing face : EnumFacing.VALUES)
		{
			BlockPos apos = pos.offset(face);
			IFluidHandler ifh = FluidUtil.getFluidHandler(world, apos, face.getOpposite());
			if(ifh instanceof FluidBlockWrapper || ifh instanceof BlockLiquidWrapper)
				continue;
			if(ifh != null && FluidHelper.canAccept(ifh, fluid))
				list.add(ifh);
		}
		return list;
	}
	
	public int emitFluid(FluidStack fluid)
	{
		if(fluid == null)
			return 0;
		
		List<IFluidHandler> list = findFLAcceptors(fluid);
		
		if(list.isEmpty())
			return 0;
		
		int per = fluid.amount / list.size();
		
		int quant = 0;
		
		for(int i = 0; i < list.size(); ++i)
		{
			IFluidHandler ies = list.get(i);
			int accepted = ies.fill(fluid.copy(), true);
			fluid.amount -= accepted;
			if(accepted < per && i != list.size() - 1)
				per = fluid.amount / (list.size() - i - 1);
			quant += accepted;
		}
		
		return quant;
	}
	
	public double emitEnergy(double EMC)
	{
		IndexedMap<EnumFacing, IEmcAcceptor> list = findEMCAcceptors();
		if(list.isEmpty())
			return 0;
		
		double ffe = EMC;
		double per = EMC / list.size();
		
		double quant = 0;
		
		int i = 0;
		for(EnumFacing ef : list.keySet())
		{
			IEmcAcceptor ies = list.get(ef);
			double accepted = ies.acceptEMC(ef, EMC / (double) list.size());
			EMC -= accepted;
			if(accepted < per && i != list.size() - 1)
				per = EMC / (list.size() - i - 1);
			quant += accepted;
			
			++i;
		}
		
		return quant;
	}
}