package tk.zeitheron.equivadditions.pipes.emc;

import moze_intel.projecte.api.tile.IEmcAcceptor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.util.Constants.NBT;

public class PipeEMCStorage implements IEmcAcceptor
{
	protected long energy;
	protected long capacity;
	protected long maxReceive;
	protected long maxExtract;
	
	public PipeEMCStorage(long capacity, long maxReceive, long maxExtract, long energy)
	{
		this.capacity = capacity;
		this.maxReceive = maxReceive;
		this.maxExtract = maxExtract;
		this.energy = Math.max(0, Math.min(capacity, energy));
	}
	
	public PipeEMCStorage(long capacity, long maxReceive, long maxExtract)
	{
		this(capacity, maxReceive, maxExtract, 0);
	}
	
	public PipeEMCStorage(long capacity, long maxTransfer)
	{
		this(capacity, maxTransfer, maxTransfer, 0);
	}
	
	public PipeEMCStorage(long capacity)
	{
		this(capacity, capacity, capacity, 0);
	}
	
	public void setEnergyStored(long energy)
	{
		this.energy = energy;
	}
	
	public long receiveEnergy(long maxReceive, boolean simulate)
	{
		if(!canReceive())
			return 0;
		long energyReceived = Math.min(capacity - energy, Math.min(this.maxReceive, maxReceive));
		if(!simulate)
			energy += energyReceived;
		return energyReceived;
	}
	
	public long extractEnergy(long maxExtract, boolean simulate)
	{
		if(!canExtract())
			return 0;
		long energyExtracted = Math.min(energy, Math.min(this.maxExtract, maxExtract));
		if(!simulate)
			energy -= energyExtracted;
		return energyExtracted;
	}
	
	public long getEnergyStored()
	{
		return energy;
	}
	
	public long getMaxEnergyStored()
	{
		return capacity;
	}
	
	public boolean canExtract()
	{
		return this.maxExtract > 0;
	}
	
	public boolean canReceive()
	{
		return this.maxReceive > 0;
	}
	
	public long getMaxExtract()
	{
		return maxExtract;
	}
	
	public long getMaxReceive()
	{
		return maxReceive;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		if(energy < 0)
			energy = 0;
		nbt.setLong("Energy", energy);
		return nbt;
	}
	
	public PipeEMCStorage readFromNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("Energy", NBT.TAG_DOUBLE))
			energy = (long) nbt.getDouble("Energy");
		else
			energy = nbt.getLong("Energy");
		if(energy > capacity)
			energy = capacity;
		return this;
	}
	
	@Override
	public long getMaximumEmc()
	{
		return capacity;
	}
	
	@Override
	public long getStoredEmc()
	{
		return energy;
	}
	
	@Override
	public long acceptEMC(EnumFacing arg0, long arg1)
	{
		return receiveEnergy(arg1, false);
	}
}