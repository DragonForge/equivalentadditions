package tk.zeitheron.equivadditions.pipes;

import java.util.List;

import com.zeitheron.hammercore.utils.math.vec.Cuboid6;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * An implementation for {@link TileEntity} that wants to be a pipe.
 */
public interface IPipe
{
	Cuboid6 CENTER = new Cuboid6(5 / 16D, 5 / 16D, 5 / 16D, 11 / 16D, 11 / 16D, 11 / 16D);
	Cuboid6 DOWN_CENTER = new Cuboid6(5.5 / 16D, 0, 5.5 / 16D, 10.5 / 16D, 5 / 16D, 10.5 / 16D);
	Cuboid6 CENTER_UP = new Cuboid6(5.5 / 16D, 10.5 / 16D, 5.5 / 16D, 10.5 / 16D, 1, 10.5 / 16D);
	Cuboid6 NORH_CENTER = new Cuboid6(5.5 / 16D, 5.5 / 16D, 0, 10.5 / 16D, 10.5 / 16D, 5 / 16D);
	Cuboid6 CENTER_SOUTH = new Cuboid6(5.5 / 16D, 5.5 / 16D, 10.5 / 16D, 10.5 / 16D, 10.5 / 16D, 1);
	Cuboid6 WEST_CENTER = new Cuboid6(0, 5.5 / 16D, 5.5 / 16D, 5 / 16D, 10.5 / 16D, 10.5 / 16D);
	Cuboid6 CENTER_EAST = new Cuboid6(10.5 / 16D, 5.5 / 16D, 5.5 / 16D, 1, 10.5 / 16D, 10.5 / 16D);
	
	boolean isConnectedTo(EnumFacing face);
	
	ResourceLocation getTex();
	
	BlockPos coordinates();
	
	World world();
	
	void kill();
	
	default void addCuboids(List<Cuboid6> cubes)
	{
		cubes.add(CENTER);
		if(isConnectedTo(EnumFacing.DOWN))
			cubes.add(DOWN_CENTER);
		if(isConnectedTo(EnumFacing.UP))
			cubes.add(CENTER_UP);
		if(isConnectedTo(EnumFacing.NORTH))
			cubes.add(NORH_CENTER);
		if(isConnectedTo(EnumFacing.SOUTH))
			cubes.add(CENTER_SOUTH);
		if(isConnectedTo(EnumFacing.WEST))
			cubes.add(WEST_CENTER);
		if(isConnectedTo(EnumFacing.EAST))
			cubes.add(CENTER_EAST);
	}
}