package tk.zeitheron.equivadditions.tiles;

import java.text.DecimalFormat;
import java.util.UUID;

import tk.zeitheron.equivadditions.InfoEA;
import tk.zeitheron.equivadditions.net.PacketSyncEMC;
import tk.zeitheron.equivadditions.pipes.emc.PipeEMCStorage;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;

import moze_intel.projecte.api.ProjectEAPI;
import moze_intel.projecte.api.capabilities.IKnowledgeProvider;
import moze_intel.projecte.api.tile.IEmcAcceptor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEMCProxy extends TileSyncableTickable implements IEmcAcceptor, ITooltipProviderHC
{
	public UUID owner;
	public String username = "Unknown";
	public PipeEMCStorage internal = new PipeEMCStorage(Long.MAX_VALUE);
	
	public TileEMCProxy(UUID owner)
	{
		this.owner = owner;
	}
	
	public TileEMCProxy()
	{
		this.owner = UUID.randomUUID();
	}
	
	boolean hasKnow;
	IKnowledgeProvider knowledge;
	
	@Override
	public void tick()
	{
		knowledge = getKnowledge();
		if((knowledge != null) != hasKnow)
			sendChangesToNearby();
		hasKnow = knowledge != null;
	}
	
	public IKnowledgeProvider getKnowledge()
	{
		if(!world.isRemote && world.getMinecraftServer() != null)
		{
			EntityPlayerMP mp = world.getMinecraftServer().getPlayerList().getPlayerByUUID(owner);
			if(mp != null)
			{
				username = mp.getGameProfile().getName();
				IKnowledgeProvider know = mp.getCapability(ProjectEAPI.KNOWLEDGE_CAPABILITY, null);
				if(know != null)
				{
					long accept = Math.min(Long.MAX_VALUE - know.getEmc(), internal.getStoredEmc());
					know.setEmc(know.getEmc() + accept);
					internal.extractEnergy(accept, false);
					if(accept > 0)
						HCNet.INSTANCE.sendTo(new PacketSyncEMC().setEmc(know.getEmc()), mp);
				}
				return know;
			}
		}
		return null;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setUniqueId("Owner", owner);
		nbt.setString("Name", username);
		internal.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		owner = nbt.getUniqueId("Owner");
		username = nbt.getString("Name");
		internal.readFromNBT(nbt);
		setTooltipDirty(true);
	}
	
	@Override
	public long getMaximumEmc()
	{
		return internal.getMaximumEmc();
	}
	
	@Override
	public long getStoredEmc()
	{
		return internal.getStoredEmc();
	}
	
	@Override
	public long acceptEMC(EnumFacing from, long amount)
	{
		long d = internal.acceptEMC(null, amount);
		if(d > 0)
			sendChangesToNearby();
		return d;
	}
	
	boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.tdirty = dirty;
	}
	
	static final DecimalFormat FORMAT = new DecimalFormat("#0.0");
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoEA.MOD_ID + ".stored"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d", Math.round(internal.getEnergyStored()))).appendColor(TextFormatting.AQUA));
		tip.append(new StringTooltipInfo(" EMC"));
		
		if(owner != null && Minecraft.getMinecraft().player.getUniqueID().equals(owner))
		{
			username = Minecraft.getMinecraft().player.getGameProfile().getName();
		}
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoEA.MOD_ID + ".owner"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(username).appendColor(TextFormatting.GREEN));
	}
}