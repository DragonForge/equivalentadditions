package tk.zeitheron.equivadditions.proxy;

import tk.zeitheron.equivadditions.blocks.BlockEMCPipe;
import tk.zeitheron.equivadditions.client.RenderPipes;
import tk.zeitheron.equivadditions.client.tesr.TESREMCPipe;
import tk.zeitheron.equivadditions.tiles.TileEMCPipe;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	public void init()
	{
		MinecraftForge.EVENT_BUS.register(new RenderPipes());
		{
			TESREMCPipe tesr = new TESREMCPipe();
			ClientRegistry.bindTileEntitySpecialRenderer(TileEMCPipe.class, tesr);
			for(Block b : GameRegistry.findRegistry(Block.class).getValuesCollection())
				if(b instanceof BlockEMCPipe)
					ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(b), tesr);
		}
	}
}