package tk.zeitheron.equivadditions.blocks;

import javax.annotation.Nonnull;

import com.zeitheron.hammercore.internal.blocks.IWitherProofBlock;

import moze_intel.projecte.gameObjs.ObjHandler;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockMatter extends Block implements IWitherProofBlock
{
	public BlockMatter()
	{
		super(Material.ROCK);
	}
	
	@Override
	public boolean canHarvestBlock(IBlockAccess world, @Nonnull BlockPos pos, @Nonnull EntityPlayer player)
	{
		ItemStack stack = player.getHeldItem(EnumHand.MAIN_HAND);
		if(!stack.isEmpty())
			return stack.getItem() == ObjHandler.rmPick || stack.getItem() == ObjHandler.rmStar || stack.getItem() == ObjHandler.rmHammer;
		return false;
	}
	
	@Override
	public float getPlayerRelativeBlockHardness(IBlockState state, EntityPlayer player, World worldIn, BlockPos pos)
	{
		float bh = super.getPlayerRelativeBlockHardness(state, player, worldIn, pos);
		if(canHarvestBlock(worldIn, pos, player))
			return bh * 12_000F;
		return bh;
	}
}