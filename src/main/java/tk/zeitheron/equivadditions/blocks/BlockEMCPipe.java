package tk.zeitheron.equivadditions.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import tk.zeitheron.equivadditions.InfoEA;
import tk.zeitheron.equivadditions.client.ParticlePipeDigging;
import tk.zeitheron.equivadditions.pipes.IPipe;
import tk.zeitheron.equivadditions.pipes.emc.EMCPipe;
import tk.zeitheron.equivadditions.tiles.TileEMCPipe;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.ICubeManager;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockEMCPipe extends BlockTraceable implements ITileBlock<TileEMCPipe>, IPipe, ITileEntityProvider, ICubeManager
{
	public final Function<TileEMCPipe, EMCPipe> pipe;
	public final int transfer;
	public final String name;
	
	public BlockEMCPipe(String name, int transfer, Function<TileEMCPipe, EMCPipe> pipe)
	{
		super(Material.ROCK);
		setTranslationKey("pipe_emc_" + name);
		setHarvestLevel("pickaxe", 0);
		setHardness(0);
		this.name = name;
		this.pipe = pipe;
		this.transfer = transfer;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(I18n.format("tooltip." + InfoEA.MOD_ID + ".transfer") + ": " + String.format("%,d", transfer) + " " + I18n.format("gui." + InfoEA.MOD_ID + ".emcpt"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, ParticleManager manager)
	{
		IPipe pipe = WorldUtil.cast(world.getTileEntity(pos), IPipe.class);
		int i = 2;
		for(int j = 0; j < i; ++j)
			for(int k = 0; k < i; ++k)
				for(int l = 0; l < i; ++l)
				{
					double d0 = (j + 0.5D) / i;
					double d1 = (k + 0.5D) / i;
					double d2 = (l + 0.5D) / i;
					manager.addEffect(new ParticlePipeDigging(world, pos.getX() + d0, pos.getY() + d1, pos.getZ() + d2, d0 - 0.5D, d1 - 0.5D, d2 - 0.5D, pipe).setBlockPos(pos));
				}
		return true;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addHitEffects(IBlockState state, World world, RayTraceResult target, ParticleManager manager)
	{
		BlockPos pos = target.getBlockPos();
		EnumFacing side = target.sideHit;
		IPipe pipe = WorldUtil.cast(world.getTileEntity(pos), IPipe.class);
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();
		AxisAlignedBB axisalignedbb = state.getBoundingBox(world, pos);
		double d0 = i + world.rand.nextDouble() * (axisalignedbb.maxX - axisalignedbb.minX - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minX;
		double d1 = j + world.rand.nextDouble() * (axisalignedbb.maxY - axisalignedbb.minY - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minY;
		double d2 = k + world.rand.nextDouble() * (axisalignedbb.maxZ - axisalignedbb.minZ - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minZ;
		if(side == EnumFacing.DOWN)
			d1 = (double) j + axisalignedbb.minY - 0.10000000149011612D;
		if(side == EnumFacing.UP)
			d1 = (double) j + axisalignedbb.maxY + 0.10000000149011612D;
		if(side == EnumFacing.NORTH)
			d2 = (double) k + axisalignedbb.minZ - 0.10000000149011612D;
		if(side == EnumFacing.SOUTH)
			d2 = (double) k + axisalignedbb.maxZ + 0.10000000149011612D;
		if(side == EnumFacing.WEST)
			d0 = (double) i + axisalignedbb.minX - 0.10000000149011612D;
		if(side == EnumFacing.EAST)
			d0 = (double) i + axisalignedbb.maxX + 0.10000000149011612D;
		manager.addEffect(new ParticlePipeDigging(world, d0, d1, d2, 0.0D, 0.0D, 0.0D, pipe).setBlockPos(pos).multiplyVelocity(0.2F).multipleParticleScaleBy(0.6F));
		return true;
	}
	
	@Override
	public boolean addLandingEffects(IBlockState state, WorldServer worldObj, BlockPos blockPosition, IBlockState iblockstate, EntityLivingBase entity, int numberOfParticles)
	{
		return true;
	}
	
	@Override
	public boolean addRunningEffects(IBlockState state, World world, BlockPos pos, Entity entity)
	{
		return true;
	}
	
	@Override
	public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player)
	{
		return true;
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		IPipe pip = WorldUtil.cast(worldIn.getTileEntity(pos), IPipe.class);
		if(pip != null)
			pip.kill();
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEMCPipe(this);
	}
	
	@Override
	public Class<TileEMCPipe> getTileClass()
	{
		return TileEMCPipe.class;
	}
	
	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return face.getAxis() == Axis.Y;
	}
	
	@Override
	public ResourceLocation getTex()
	{
		return new ResourceLocation(InfoEA.MOD_ID, "textures/pipe/pipe_emc_" + name + ".png");
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public void kill()
	{
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	private static final ThreadLocal<List<Cuboid6>> cubeLists = ThreadLocal.withInitial(ArrayList::new);
	
	@Override
	public Cuboid6[] getCuboids(World world, BlockPos pos, IBlockState state)
	{
		IPipe pipe = WorldUtil.cast(world.getTileEntity(pos), IPipe.class);
		if(pipe != null)
		{
			List<Cuboid6> c = cubeLists.get();
			c.clear();
			pipe.addCuboids(c);
			return c.toArray(new Cuboid6[c.size()]);
		}
		return new Cuboid6[0];
	}
	
	@Override
	public BlockPos coordinates()
	{
		return BlockPos.ORIGIN;
	}
	
	@Override
	public World world()
	{
		return null;
	}
}