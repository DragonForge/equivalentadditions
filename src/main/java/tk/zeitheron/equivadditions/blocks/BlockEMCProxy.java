package tk.zeitheron.equivadditions.blocks;

import tk.zeitheron.equivadditions.tiles.TileEMCProxy;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockEMCProxy extends BlockDeviceHC<TileEMCProxy>
{
	public BlockEMCProxy()
	{
		super(Material.IRON, TileEMCProxy.class, "emc_proxy");
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		TileEMCProxy proxy = WorldUtil.cast(worldIn.getTileEntity(pos), TileEMCProxy.class);
		if(proxy == null)
		{
			proxy = new TileEMCProxy(placer.getUniqueID());
			worldIn.setTileEntity(pos, proxy);
		}
		proxy.owner = placer.getUniqueID();
	}
}