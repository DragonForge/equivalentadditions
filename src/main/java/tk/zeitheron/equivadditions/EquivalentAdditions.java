package tk.zeitheron.equivadditions;

import tk.zeitheron.equivadditions.init.BlocksEA;
import tk.zeitheron.equivadditions.init.ItemsEA;
import tk.zeitheron.equivadditions.proxy.CommonProxy;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import moze_intel.projecte.api.event.EMCRemapEvent;
import moze_intel.projecte.emc.EMCMapper;
import moze_intel.projecte.emc.FuelMapper;
import moze_intel.projecte.emc.SimpleStack;
import moze_intel.projecte.utils.Constants;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Mod(modid = InfoEA.MOD_ID, name = "Equivalent Additions", version = "@VERSION@", dependencies = "required-after:hammercore;required-after:projecte")
public class EquivalentAdditions
{
	public static final Logger LOG = LogManager.getLogger(InfoEA.MOD_ID);
	
	public static final int ZFUEL_BURN_TIME = Constants.AETERNALIS_BURN_TIME * 4;
	public static final int COLLECTOR_MK4_GEN = 120;
	public static final int COLLECTOR_MK4_MAX = 222_000;
	
	public static final CreativeTabs TAB = new CreativeTabs(InfoEA.MOD_ID)
	{
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(BlocksEA.PIPE_EMC_MK1);
		}
	};
	
	@SidedProxy(serverSide = "tk.zeitheron.equivadditions.proxy.CommonProxy", clientSide = "tk.zeitheron.equivadditions.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		SimpleRegistration.registerFieldBlocksFrom(BlocksEA.class, InfoEA.MOD_ID, TAB);
		SimpleRegistration.registerFieldItemsFrom(ItemsEA.class, InfoEA.MOD_ID, TAB);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
	
	@SubscribeEvent
	public void handleFuel(FurnaceFuelBurnTimeEvent e)
	{
		ItemStack stack = e.getItemStack();
		if(stack.getItem() == ItemsEA.ZEITHERON_FUEL)
			e.setBurnTime(ZFUEL_BURN_TIME);
		if(stack.getItem() == Item.getItemFromBlock(BlocksEA.ZEITHERON_FUEL_BLOCK))
			e.setBurnTime(ZFUEL_BURN_TIME * 9);
	}
	
	@SubscribeEvent
	public void emcRemap(EMCRemapEvent evt)
	{
		List<SimpleStack> FUEL_MAP = null;
		Field f = FuelMapper.class.getDeclaredFields()[0];
		f.setAccessible(true);
		try
		{
			FUEL_MAP = (List<SimpleStack>) f.get(null);
		} catch(IllegalArgumentException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
		
		if(FUEL_MAP == null)
			return;
		
		List<SimpleStack> backup = new ArrayList<>(FUEL_MAP);
		
		List<SimpleStack> fmap_ = FUEL_MAP;
		
		new Thread(() ->
		{
			List<SimpleStack> fmap = fmap_;
			
			wh: while(true)
			{
				if(fmap.size() != backup.size())
					break;
				try
				{
					for(int i = 0; i < fmap.size(); ++i)
						if(fmap.get(i) != backup.get(i))
							break wh;
				} catch(IndexOutOfBoundsException iofbe)
				{
				}
			}
			
			try
			{
				Thread.sleep(500L);
			} catch(InterruptedException e)
			{
			}
			
			synchronized(fmap)
			{
				fmap.add(new SimpleStack(new ItemStack(ItemsEA.ZEITHERON_FUEL)));
				fmap.add(new SimpleStack(new ItemStack(BlocksEA.ZEITHERON_FUEL_BLOCK)));
				
				fmap.sort(Comparator.comparing(EMCMapper::getEmcValue));
			}
		}).start();
	}
}