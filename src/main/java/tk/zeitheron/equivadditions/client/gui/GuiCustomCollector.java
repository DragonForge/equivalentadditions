package tk.zeitheron.equivadditions.client.gui;

import tk.zeitheron.equivadditions.inventory.ContainerCollectorDef;
import tk.zeitheron.equivadditions.tiles.TileCustomCollector;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.utils.RenderUtil;

import moze_intel.projecte.utils.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiCustomCollector extends GuiWTFMojang<ContainerCollectorDef>
{
	private static final ResourceLocation texture = new ResourceLocation("projecte", "textures/gui/collector3.png");
	private final TileCustomCollector tile;
	
	public GuiCustomCollector(InventoryPlayer invPlayer, TileCustomCollector tile)
	{
		super(new ContainerCollectorDef(invPlayer, tile));
		this.tile = tile;
		this.xSize = 218;
		this.ySize = 165;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int var1, int var2)
	{
		this.fontRenderer.drawString(Long.toString(getContainer().emc), 91, 32, 4210752);
		
		double kleinCharge = getContainer().kleinEmc;
		if(kleinCharge > 0)
			this.fontRenderer.drawString(Constants.EMC_FORMATTER.format(kleinCharge), 91, 44, 4210752);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3)
	{
		GlStateManager.color(1, 1, 1, 1);
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		
		this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
		
		// Light Level. Max is 12
		double progress = (int) (getContainer().sunLevel * 12.0 / 16);
		
		RenderUtil.drawTexturedModalRect(x + 160, y + 49 - progress, 220, 13 - progress, 12, progress);
		
		// EMC storage. Max is 48
		progress = getContainer().emc / tile.getMaximumEmc() * 48;
		RenderUtil.drawTexturedModalRect(x + 98, y + 18, 0, 166, progress, 10);
		
		// Klein Star Charge Progress. Max is 48
		progress = getContainer().kleinChargeProgress * 48;
		RenderUtil.drawTexturedModalRect(x + 98, y + 58, 0, 166, progress, 10);
		
		// Fuel Progress. Max is 24.
		progress = getContainer().fuelProgress * 24;
		RenderUtil.drawTexturedModalRect(x + 172, y + 55 - progress, 219, 38 - progress, 10, progress + 1);
	}
}