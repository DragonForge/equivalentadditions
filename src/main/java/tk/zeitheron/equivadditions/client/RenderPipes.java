package tk.zeitheron.equivadditions.client;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import tk.zeitheron.equivadditions.InfoEA;
import tk.zeitheron.equivadditions.pipes.IPipe;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.TextureAtlasSpriteFull;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RenderPipes
{
	public static final TextureAtlasSprite fullTex = TextureAtlasSpriteFull.sprite;
	
	public static final TextureAtlasSpriteUV coreTex = new TextureAtlasSpriteUV(1, 1, 6, 6, 16, 16);
	public static final TextureAtlasSpriteUV connectRightTex = new TextureAtlasSpriteUV(9, 1, 5, 5, 16, 16);
	public static final TextureAtlasSpriteUV connectDownTex = new TextureAtlasSpriteUV(1, 9, 5, 5, 16, 16);
	public static final TextureAtlasSpriteUV connectInnerTex = new TextureAtlasSpriteUV(9, 9, 5, 5, 16, 16);
	
	public static final List<ResourceLocation> pipeLocations = new ArrayList<>();
	public static final List<List<IPipe>> pipeValues = new ArrayList<>();
	
	public static void addPipe(IPipe pipe)
	{
		if(pipe == null)
			return;
		int i = pipeLocations.indexOf(pipe.getTex());
		if(i == -1)
		{
			i = pipeLocations.size();
			pipeLocations.add(pipe.getTex());
			ArrayList<IPipe> al = new ArrayList<>();
			al.add(pipe);
			pipeValues.add(al);
		} else
			pipeValues.get(i).add(pipe);
	}
	
	@SubscribeEvent
	public void renderWorldLast(RenderWorldLastEvent rwle)
	{
		if(rwle != null)
			return;
		GlStateManager.pushMatrix();
		
		GlStateManager.disableLighting();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.enableBlend();
		GlStateManager.enableAlpha();
		
		GlStateManager.shadeModel(Minecraft.isAmbientOcclusionEnabled() ? 7425 : 7424);
		
		RenderBlocks rb = RenderBlocks.forMod(InfoEA.MOD_ID);
		Tessellator tess = Tessellator.getInstance();
		
		for(int i = 0; i < pipeValues.size(); ++i)
		{
			List<IPipe> layer = pipeValues.get(i);
			if(layer.isEmpty())
				continue;
			GL11.glPushMatrix();
			UtilsFX.bindTexture(pipeLocations.get(i));
			
			for(IPipe pipe : layer)
			{
				tess.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
				
				int o = getBrightnessForRB(pipe, rb);
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				renderPipe(pipe, rb, o, 1, 1, 1, 1F);
				
				tess.draw();
			}
			
			layer.clear();
			GL11.glPopMatrix();
		}
		
		GlStateManager.popMatrix();
	}
	
	protected int getBrightnessForRB(IPipe te, RenderBlocks rb)
	{
		return te != null ? rb.setLighting(te.world(), te.coordinates()) : rb.setLighting(Minecraft.getMinecraft().world, Minecraft.getMinecraft().player.getPosition());
	}
	
	private void renderPipe(IPipe pipe, RenderBlocks rb, int bright, float r, float g, float b, float a)
	{
		double x = -1 / 16. + 5 / 16D;
		double y = -1 / 16. + 5 / 16D;
		double z = -1 / 16. + 5 / 16D;
		
		BlockPos pos = pipe.coordinates();
		
		x += pos.getX() - TileEntityRendererDispatcher.staticPlayerX;
		y += pos.getY() - TileEntityRendererDispatcher.staticPlayerY;
		z += pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ;
		
		// Core
		
		rb.setRenderBounds(1 / 16D, 1 / 16D, 1 / 16D, 7 / 16D, 7 / 16D, 7 / 16D);
		rb.renderFaceYNeg(x, y, z, RenderPipes.fullTex, r, g, b, bright);
		rb.renderFaceYPos(x, y, z, RenderPipes.fullTex, r, g, b, bright);
		
		rb.setRenderBounds(1 / 16D, 9 / 16D, 1 / 16D, 7 / 16D, 15 / 16D, 7 / 16D);
		rb.renderFaceXNeg(x, y - 8 / 16D, z, RenderPipes.fullTex, r, g, b, bright);
		rb.renderFaceXPos(x, y - 8 / 16D, z, RenderPipes.fullTex, r, g, b, bright);
		rb.renderFaceZNeg(x, y - 8 / 16D, z, RenderPipes.fullTex, r, g, b, bright);
		rb.renderFaceZPos(x, y - 8 / 16D, z, RenderPipes.fullTex, r, g, b, bright);
		
		// Down
		if(pipe.isConnectedTo(EnumFacing.DOWN))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 1 / 16D, 6 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 6 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 6 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZNeg(x + .5 / 16, y - 6 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZPos(x + .5 / 16, y - 6 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x - 7.5 / 16, y - 6 / 16D, z - 7.5 / 16, RenderPipes.fullTex, r, g, b, bright);
		}
		
		// Up
		if(pipe.isConnectedTo(EnumFacing.UP))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 1 / 16D, 6 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y + 5 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y + 5 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZNeg(x + .5 / 16, y + 5 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZPos(x + .5 / 16, y + 5 / 16D, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYPos(x - 7.5 / 16, y + 5 / 16D, z - 7.5 / 16, RenderPipes.fullTex, r, g, b, bright);
		}
		
		// North
		if(pipe.isConnectedTo(EnumFacing.NORTH))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 9 / 16D, 6 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x + .5 / 16, y - .5 / 16, z - 13. / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceYPos(x + .5 / 16, y - .5 / 16, z - 13. / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(1 / 16D, 10 / 16D, 9 / 16D, 6 / 16D, 15 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 8.5 / 16, z - 13. / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 8.5 / 16, z - 13. / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceZNeg(x - 7.5 / 16, y - .5 / 16, z - 13. / 16, RenderPipes.fullTex, r, g, b, bright);
		}
		
		// South
		if(pipe.isConnectedTo(EnumFacing.SOUTH))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 9 / 16D, 6 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x + .5 / 16, y - .5 / 16, z - 2. / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceYPos(x + .5 / 16, y - .5 / 16, z - 2. / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(1 / 16D, 10 / 16D, 9 / 16D, 6 / 16D, 15 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 8.5 / 16, z - 2. / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 8.5 / 16, z - 2. / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceZPos(x - 7.5 / 16, y - .5 / 16, z - 2. / 16, RenderPipes.fullTex, r, g, b, bright);
		}
		
		// East
		if(pipe.isConnectedTo(EnumFacing.EAST))
		{
			rb.setRenderBounds(9 / 16D, 2 / 16D, 1 / 16D, 14 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceYNeg(x - 2. / 16, y - .5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceYPos(x - 2. / 16, y - .5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 10 / 16D, 1 / 16D, 14 / 16D, 15 / 16D, 6 / 16D);
			rb.renderFaceZNeg(x - 2. / 16, y - 8.5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZPos(x - 2. / 16, y - 8.5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceXPos(x - 2. / 16, y - .5 / 16, z - 7.5 / 16, RenderPipes.fullTex, r, g, b, bright);
		}
		
		// West
		if(pipe.isConnectedTo(EnumFacing.WEST))
		{
			rb.setRenderBounds(9 / 16D, 2 / 16D, 1 / 16D, 14 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceYNeg(x - 13. / 16, y - .5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceYPos(x - 13. / 16, y - .5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 10 / 16D, 1 / 16D, 14 / 16D, 15 / 16D, 6 / 16D);
			rb.renderFaceZNeg(x - 13. / 16, y - 8.5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			rb.renderFaceZPos(x - 13. / 16, y - 8.5 / 16, z + .5 / 16, RenderPipes.fullTex, r, g, b, bright);
			
			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x - 13. / 16, y - .5 / 16, z - 7.5 / 16, RenderPipes.fullTex, r, g, b, bright);
		}
	}
}