package tk.zeitheron.equivadditions.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import moze_intel.projecte.api.ProjectEAPI;
import moze_intel.projecte.api.capabilities.IKnowledgeProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketSyncEMC implements IPacket
{
	static
	{
		IPacket.handle(PacketSyncEMC.class, PacketSyncEMC::new);
	}
	
	public double emc;
	
	public PacketSyncEMC setEmc(double emc)
	{
		this.emc = emc;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("EMC", emc);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		emc = nbt.getDouble("EMC");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		IKnowledgeProvider prov = Minecraft.getMinecraft().player.getCapability(ProjectEAPI.KNOWLEDGE_CAPABILITY, null);
		if(prov != null)
			prov.setEmc(emc);
		return null;
	}
}