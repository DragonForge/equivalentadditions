package tk.zeitheron.equivadditions.init;

import tk.zeitheron.equivadditions.EquivalentAdditions;
import tk.zeitheron.equivadditions.InfoEA;
import tk.zeitheron.equivadditions.blocks.BlockCustomCollector;
import tk.zeitheron.equivadditions.blocks.BlockEMCPipe;
import tk.zeitheron.equivadditions.blocks.BlockEMCProxy;
import tk.zeitheron.equivadditions.blocks.BlockMatter;
import tk.zeitheron.equivadditions.pipes.emc.impl.SimpleEMCPipe;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.ResourceLocation;

public class BlocksEA
{
	public static final BlockEMCPipe PIPE_EMC_MK1 = new BlockEMCPipe("mk1", 256, tile -> new SimpleEMCPipe(tile, new ResourceLocation(InfoEA.MOD_ID, "textures/pipe/pipe_emc_mk1.png"), 256));
	public static final BlockEMCPipe PIPE_EMC_MK2 = new BlockEMCPipe("mk2", 1024, tile -> new SimpleEMCPipe(tile, new ResourceLocation(InfoEA.MOD_ID, "textures/pipe/pipe_emc_mk2.png"), 1024));
	public static final BlockEMCPipe PIPE_EMC_MK3 = new BlockEMCPipe("mk3", 4096, tile -> new SimpleEMCPipe(tile, new ResourceLocation(InfoEA.MOD_ID, "textures/pipe/pipe_emc_mk3.png"), 4096));
	public static final BlockEMCPipe PIPE_EMC_MK4 = new BlockEMCPipe("mk4", 16384, tile -> new SimpleEMCPipe(tile, new ResourceLocation(InfoEA.MOD_ID, "textures/pipe/pipe_emc_mk4.png"), 16384));
	
	public static final Block ZEITHERON_FUEL_BLOCK = new Block(Material.ROCK).setHardness(2F).setTranslationKey("zeitheron_fuel_block");
	public static final BlockMatter BLUE_MATTER_BLOCK = (BlockMatter) new BlockMatter().setHardness(3_000_000F).setResistance(Float.POSITIVE_INFINITY).setTranslationKey("blue_matter_block");
	
	public static final BlockCustomCollector ENERGY_COLLECTOR_MK4 = new BlockCustomCollector(EquivalentAdditions.COLLECTOR_MK4_MAX, EquivalentAdditions.COLLECTOR_MK4_GEN, 4);
	
	public static final BlockEMCProxy EMC_PROXY = new BlockEMCProxy();
}